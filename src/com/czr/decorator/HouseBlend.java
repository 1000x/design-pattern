package com.czr.decorator;

/**
 * @Description 具体组件
 * @author chenzhiren
 * @date 2018年2月1日
 */
public class HouseBlend extends Beverage {

    
    
    public HouseBlend() {
        description = "house blend coffee";
    }

    @Override
    public double cost() {
        return .89;
    }

}
