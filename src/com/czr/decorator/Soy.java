package com.czr.decorator;

/**
 * @Description 具体装饰者
 * @author chenzhiren
 * @date 2018年2月1日
 */
public class Soy extends CondimentDecorator {

    Beverage beverage;
    
    public Soy(Beverage beverage) {
       this.beverage = beverage; 
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ",soy";
    }

    @Override
    public double cost() {
        return .35+beverage.cost();
    }

}
