package com.czr.decorator;

/**
 * @Description 抽象装饰者
 * @author chenzhiren
 * @date 2018年2月1日
 */
public abstract class CondimentDecorator extends Beverage {
    
    public abstract String getDescription();
}
