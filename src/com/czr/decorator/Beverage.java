package com.czr.decorator;

/**
 * @Description 抽象组件
 *  通常情况下应该将该类设计为接口，但是根据需求已经被设计成了抽象类，故保持不变，尽量维持原有代码接口
 * @author chenzhiren
 * @date 2018年2月1日
 */
public abstract class Beverage {
    
    String description = "unknown";

    public String getDescription() {
        return description;
    }
    
    public abstract double cost();
}
