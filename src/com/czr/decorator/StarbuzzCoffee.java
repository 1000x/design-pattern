package com.czr.decorator;

/**
 * @Description 测试
 * @author chenzhiren
 * @date 2018年2月1日
 */
public class StarbuzzCoffee {

    public static void main(String[] args) {
        Beverage beverage = new Esspresso();
        System.out.println(beverage.getDescription() + "$" +beverage.cost());
        
        Beverage beverage2 = new Esspresso();
        beverage2 = new Mocha(beverage2);
        beverage2 = new Mocha(beverage2);
        beverage2 = new Soy(beverage2);
        System.out.println(beverage2.getDescription() +"$"+beverage2.cost());
        
        Beverage beverage3 = new HouseBlend();
        beverage3 = new Mocha(beverage3);
        beverage3 = new Soy(beverage3);
        beverage3 = new Whip(beverage3);
        System.out.println(beverage3.getDescription() +"$"+ beverage3.cost());
    }
}
