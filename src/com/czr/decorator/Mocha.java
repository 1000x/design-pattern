package com.czr.decorator;

/**
 * @Description 具体装饰者
 * @author chenzhiren
 * @date 2018年2月1日
 */
public class Mocha extends CondimentDecorator {
    
    //  每个装饰者都持有抽象组件对象
    Beverage beverage;
    
    public Mocha(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ",mocha";
    }

    @Override
    public double cost() {
        return .20+beverage.cost();
    }

}
