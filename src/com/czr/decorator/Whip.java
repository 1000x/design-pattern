package com.czr.decorator;

/**
 * @Description 具体装饰者
 * @author chenzhiren
 * @date 2018年2月1日
 */
public class Whip extends CondimentDecorator {

    Beverage beverage;
    
    public Whip(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ",whip";
    }

    @Override
    public double cost() {
        return .40+beverage.cost();
    }

}
