package com.czr.decorator;

/**
 * @Description 具体组件
 * @author chenzhiren
 * @date 2018年2月1日
 */
public class Esspresso extends Beverage {

    public Esspresso() {
        description = "浓咖啡";
    }
    
    @Override
    public double cost() {
        return 1.29;
    }
    
}
