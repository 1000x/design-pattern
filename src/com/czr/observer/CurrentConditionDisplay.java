package com.czr.observer;

public class CurrentConditionDisplay implements Observer, DisplayElement {

    private float temp;
    private float humidity;
    private Subject weatherData;
    
    public CurrentConditionDisplay(Subject weatherData) {
        this.weatherData =weatherData;
        weatherData.registerObserver(this);
    }
    
    @Override
    public void display() {
        System.out.println("CurrentCondition:" + temp 
            +"F degrees and "+ humidity + "% humidity");
    }

    @Override
    public void update(float temp, float humidity, float perssure) {
        this.temp = temp;
        this.humidity = humidity;
        display();

    }

}
