package com.czr.observer.jdk;

import com.czr.observer.jdk.CurrentConditionDisplay;
import com.czr.observer.jdk.WeatherData;

public class MyTest {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        CurrentConditionDisplay conditionDisplay = 
            new CurrentConditionDisplay(weatherData);
        weatherData.setMeasurements(43, 12, 50.f);
        conditionDisplay.update(weatherData, null);
        weatherData.setMeasurements(83, 72, 20.f);
        conditionDisplay.update(weatherData, null);
        weatherData.setMeasurements(60, 90, 40.f);
        conditionDisplay.update(weatherData, null);
    }
}
