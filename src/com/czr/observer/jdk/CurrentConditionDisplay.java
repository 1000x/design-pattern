package com.czr.observer.jdk;

import java.util.Observable;
import java.util.Observer;

import com.czr.observer.DisplayElement;

public class CurrentConditionDisplay implements Observer, DisplayElement {

    Observable Observable;
    private float temp;
    private float humidity;
    
    
    // 注册为观察者
    public CurrentConditionDisplay(java.util.Observable observable) {
        this.Observable = observable;
        observable.addObserver(this);
    }

    @Override
    public void display() {
        System.out.println("CurrentCondition:" + temp 
                +"F degrees and "+ humidity + "% humidity");
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData weatherData = (WeatherData) o;
            this.temp = weatherData.getTemp();
            this.humidity = weatherData.getHumidity();
            display();
        }

    }

}
