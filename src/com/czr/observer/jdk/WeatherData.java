package com.czr.observer.jdk;

import java.util.Observable;

public class WeatherData extends Observable {

    private float temp;
    private float humidity;
    private float pressure;
    
    public WeatherData() {
        // TODO Auto-generated constructor stub
    }
    
    // 没有通知观察者，采用的是观察者主动拉取数据的方式
    public void measurementsChanged() {
        setChanged();
    }
    
    public void setMeasurements(float temp,float humidity,float pressure) {
        this.temp = temp;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    public float getTemp() {
        return temp;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getPressure() {
        return pressure;
    }
    
    
}
