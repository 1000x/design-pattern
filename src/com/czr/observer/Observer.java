package com.czr.observer;

/**
 * @Description 抽象观察者 所有的具体观察者都必须实现此接口
 * @author chenzhiren
 * @date 2018年1月31日
 */
public interface Observer {
    
    void update(float temp,float humidity,float perssure);
}
