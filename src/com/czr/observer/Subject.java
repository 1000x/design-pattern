package com.czr.observer;

/**
 * @Description 抽象观察主题
 * @author chenzhiren
 * @date 2018年1月31日
 */
public interface Subject {
    
    void registerObserver(Observer ob);
    void removeObserver(Observer ob);
    void notifyObservers();
}
