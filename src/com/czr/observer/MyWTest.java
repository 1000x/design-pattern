package com.czr.observer;

public class MyWTest {
    
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        CurrentConditionDisplay conditionDisplay = 
            new CurrentConditionDisplay(weatherData);
        weatherData.setMeasurements(43, 12, 50.f);
        weatherData.setMeasurements(83, 72, 20.f);
        weatherData.setMeasurements(60, 90, 40.f);
    }
}
