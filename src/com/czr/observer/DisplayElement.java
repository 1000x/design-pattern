package com.czr.observer;

/**
 * @Description 显示天气数据
 * @author chenzhiren
 * @date 2018年1月31日
 */
public interface DisplayElement {
    
    void display();
}
