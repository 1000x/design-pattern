package com.czr.strategy;

public class SqueakQuack implements QuackBehaviour {

    @Override
    public void quack() {
        System.out.println("squack");
    }

}
