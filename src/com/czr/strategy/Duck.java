package com.czr.strategy;

public abstract class Duck {
    
    FlyBehaviour flyBehaviour;
    QuackBehaviour quackBehaviour;
    
    public Duck() {
        // TODO Auto-generated constructor stub
    }
    
    public abstract void display();
    
    public void performFly() {
        flyBehaviour.fly();
    }
    
    public void perfromQuack() {
        quackBehaviour.quack();
    }
    
    public void setFlyBehaviour(FlyBehaviour fb) {
        flyBehaviour = fb;
    }
    public void setQuackBehaviour(QuackBehaviour qb) {
        quackBehaviour = qb;
    }
}
