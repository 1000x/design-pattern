package com.czr.strategy;

public class MallardDuck extends Duck {

    public MallardDuck() {
        flyBehaviour = new FlyWithWings();
        quackBehaviour = new MuteQuack();
    }
    
    @Override
    public void display() {
        System.out.println("iam really a duck");
    }

}
