package com.czr.strategy;

public interface QuackBehaviour {

    void quack();

}
