package com.czr.strategy;

public class FlyNoWay implements FlyBehaviour {

    @Override
    public void fly() {
        System.out.println("i cannot fly");
    }

}
